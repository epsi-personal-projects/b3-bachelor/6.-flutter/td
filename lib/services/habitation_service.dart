import 'package:untitled/models/habitation.dart';
import 'package:untitled/models/habitations_data.dart';
import 'package:untitled/models/type_habitat.dart';
import 'package:untitled/models/typehabitat_data.dart';

class HabitationService {
  var _typeHabitats;
  var _habitations;

  HabitationService() {
    _typeHabitats = TypehabitatData.buildList();
    _habitations = HabitationsData.buildList();
  }

  List<TypeHabitat> getTypeHabitats() {
    return _typeHabitats;
  }

  List<Habitation> getHabitationTop10() {
    return _habitations
        .where((e) => e.id % 2 == 1)
        .take(10)
        .toList();
  }

  List<Habitation> getMaisons() {
    return _getHabitations(isHouse: true);
  }

  List<Habitation> getAppartements() {
    return _getHabitations(isHouse: false);
  }

  List<Habitation> _getHabitations({bool isHouse = true}) {
    return _habitations
        .where((e) => e.typeHabitat.id == (isHouse ? 1 : 2))
        .toList();
  }
}